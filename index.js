const fs = require('fs');

console.log(`## Commands
use below Commands after node . , like (node . login Alice)
login [name] - Logs in as this customer and creates the customer if not exist
deposi [amount] - Deposits this amount to the logged in customer
withdrw [amount] - Withdraws this amount from the logged in customer
transfr [target] [amount] - Transfers this amount from the logged in customer to the target customer
logout - Logs out of the current customer
..............................................................................................................`);
const file = 'data.json';
 if(!fs.existsSync(file)){
    fs.writeFileSync(file, JSON.stringify({}));
 } 

 const data = JSON.parse(fs.readFileSync(file));
const [command, arg, arg2]= process.argv.slice(2);
if(command) {
    switch (command) {
        case 'login':
            data.loggedInUser = arg;
            console.log(`Hello, ${arg}
            Your balance is ${data.loggedInUser.amount || 0}`)
            break;
        case 'deposit':
            if(!data[data.loggedInUser]) {
            data[data.loggedInUser] = { amount: 0 }    
            } console.log(data)
            data[data.loggedInUser].amount += parseInt(arg)
            console.log(`Your balance is`, data[data.loggedInUser].amount);
            break;
        case 'withdraw':
            data[data.loggedInUser].amount = data[data.loggedInUser].amount- parseInt(arg)
            console.log(`Your balance is`, data[data.loggedInUser].amount);
            break;
        case 'transfer':
            console.log("why i am called")
            data[data.loggedInUser].amount = data[data.loggedInUser].amount- parseInt(arg2);
            data[arg].amount = data[arg].amount+ parseInt(arg2);
            console.log(`Your balance is`, data[data.loggedInUser].amount);
            break;
        case 'logout':
            console.log(`Goodbye, ${data.loggedInUser}!`);
            data.loggedInUser = '';
            break;
        default:
            break;
    }
    fs.writeFileSync(file, JSON.stringify(data));
}